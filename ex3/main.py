from tabulate import tabulate
import datetime, csv

class CustomDate:
    def __init__(self, day, month, year):
        self.day = day
        self.month = month
        self.year = year

    def __eq__(self, value):
        return self.day == value.day and self.month == value.month and self.year == value.year

    def __lt__(self, value):
        if self.year < value.year:
            return True
        elif self.month < value.month:
            return True
        elif self.day < value.day:
            return True
        return False

    def parse_to_date_py(self):
        return datetime.date(self.year, self.month, self.day)

 class Student:
    def __init__(self, firstname, lastname, birthday_date):
        self.firstname = firstname
        self.lastname = lastname
        self.birthday_date = birthday_date

    def get_email(self):
        return self.firstname + "." + self.lastname + "@etu.univ-tours.fr"

    def get_age(self):

        age = datetime.date.today() - self.birthday_date.parse_to_date_py()
        return int(age.days / 365.25)

d1 = CustomDate(25, 9, 1998)
d2 = CustomDate(26, 9, 1998)
print("d1 < d2", d1 < d2)
print("d2 == d2", d2 == d2)
print("d2 < d1", d2 < d1)
print("d2 == d1", d2 == d1)
print("-------------------------")

justin = Student("Justin", "MARTIN", CustomDate(25, 9, 1998))
print("age: ", justin.get_age())
print("email: ", justin.get_email())
print("-------------------------")

listStudent = []
with open('./ex3/fichetu.csv') as csvfile:
    reader = csv.reader(csvfile, delimiter=";")
    for row in reader:
        date = row[2].split("/")
        dateFormatted = CustomDate(int(date[0]), int(date[1]), int(date[2]))
        listStudent.append(Student(row[1], row[0], dateFormatted))

listStudentInfo = [[student.get_email(), student.get_age()] for student in listStudent]

print(tabulate(listStudentInfo, headers=["Email", "Age"]))