complet_menu_as_string = ("\nMenu : \n\n"
                          "1. Choisir un nom de fichier \n"
                          "2. Ajouter un texte \n"
                          "3. Afficher Le fichier complet \n"
                          "4. Vider le fichier \n"
                          "5. Quitter le programme")
small_menu_as_string = ("\nSelectionner une option : \n\n"
                        "1. Choisir un nom de fichier \n"
                        "2. Quitter le programme")

file_selected = False
filepath = ""

def get_menu():
    return complet_menu_as_string if file_selected else small_menu_as_string

def get_available_option():
    return ["1", "2", "3", "4", "5"] if file_selected else ["1", "2"]

def choice_file():
    global file_selected, filepath
    filepath = input("\nChemin d'accès au fichier : ")
    file_selected = True

def leave():
    quit(0)

def add_text():
    text_to_add = input("\nIndiquer le texte à ajouter : ")
    file = open(filepath, "a")
    file.write(text_to_add)
    file.close()

def show_file():
    print("------ file content -------")
    file = open(filepath, "r")
    print(file.read())

def clear_file():
    open(filepath, 'w').close()

def get_function():
    return {"1": choice_file,
            "2": add_text,
            "3": show_file,
            "4": clear_file,
            "5": leave
            } if file_selected else {"1": choice_file, "2": leave}

while True:
    print(get_menu())
    choice = input("\nChoix : ")

    if choice in get_available_option():
        map_function = get_function()
        map_function[choice]()
